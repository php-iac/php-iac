<?php

namespace PHPIAC;

use PHPIAC\Command\RunCommand;
use Symfony\Component\Console\Application;

class IAC
{
    public function __construct()
    {
        $application = new Application();

        $defaultCommand = new RunCommand();
        $application->add($defaultCommand);
        $application->setDefaultCommand($defaultCommand->getName());

        # ... other commands here

        $application->run();
    }
}
