<?php

namespace PHPIAC\Command;

use PHPIAC\Connection;
use PHPIAC\Task;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\FormatterHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RunCommand extends Command
{
    protected static $defaultName = 'run';

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->addOption('config', 'c', InputOption::VALUE_REQUIRED, 'Path to config file', getcwd() . '/config.php');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $config = include $input->getOption('config');

        Connection::initialize($config['host'], $config['user'], $config['private_key_file']);

        /**@var FormatterHelper $formater*/
        $formater = $this->getHelper('formatter');

        foreach ($config['tasks'] as $task) {
            /**@var Task $task*/

            $output->writeln($task->getName());
            if (! $task->module->checkState()) {
                $output->writeln('Running');

                $task->module->execute();
            }
            else {
                $output->writeln('Skipping');
            }

            $output->writeln('- - -');
        }

        return Command::SUCCESS;
    }
}
