<?php

namespace PHPIAC;

use PHPIAC\Module\ModuleInterface;

class Task
{
    public string $name = '';
    public ModuleInterface $module;

    /**
     * Task constructor.
     */
    public function __construct() {}

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        if (! empty($this->name)) {
            return $this->name;
        }

        $moduleReflection = new \ReflectionClass($this->module);
        $firstProperty = $moduleReflection->getProperties()[0];
        $firstProperty->setAccessible(true);

        return $moduleReflection->getShortName() . ' - ' . $firstProperty->getValue($this->module);
    }

    /**
     * @param ModuleInterface $module
     *
     * @return $this
     */
    public function setModule(ModuleInterface $module): self
    {
        $this->module = $module;

        return $this;
    }
}
