<?php

namespace PHPIAC\Module;

class State
{
    public const PRESENT = 'present';
    public const ABSENT = 'absent';
    public const ENABLED = 'enabled';
    public const DISABLED = 'disabled';
}
