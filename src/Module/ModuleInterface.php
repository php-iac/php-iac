<?php

namespace PHPIAC\Module;

interface ModuleInterface
{
    /**
     * @return bool
     */
    public function checkState(): bool;

    /**
     * @return void
     */
    public function execute(): void;
}
