<?php

namespace PHPIAC\Module;

abstract class Module implements ModuleInterface
{
    /**
     * Module constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        foreach ($config as $key => $value) {
            $this->$key = $value;
        }
    }
}
